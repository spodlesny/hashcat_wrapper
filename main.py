import json
import subprocess
import time

import psutil as psutil
import requests
import timer as timer


class HashcatWrapper():
    def __init__(self):
        self.hashcat_instance = None
        self.api_url = "http://internat.spodlesny.eu:8000"
        self.already_sent_results = []

    def add_new_hash_into_pool(self, new_hash: str):
        file = open("hash_pool.txt", mode="a+")
        file.seek(0)
        if new_hash + "\n" not in file.readlines():
            file.write(new_hash + "\n")
            file.flush()
            print("New hash '{}' has been inserted into hashpool".format(new_hash))
        file.close()
        requests.post(data=json.dumps({"hash": new_hash, "status": "Cracking..."}),
                      url=self.api_url + "/edit_hash_status")

    def get_orders(self) -> str:
        return requests.get(self.api_url + "/hashcat_control").json()[0].get("fields").get("command")

    def is_hashcat_running(self):
        """
            Check if there is any running process that contains the given name process_name.
            #source: https://thispointer.com/python-check-if-a-process-is-running-by-name-and-find-its-process-id-pid/
        """
        # Iterate over the all the running process
        process_name = "hashcat"
        for proc in psutil.process_iter():
            try:
                # Check if process name contains the given name string.
                if process_name.lower() in proc.name().lower():
                    print("Process status is: {}".format(proc.status()))
                    if proc.status() == "zombie":
                        return False
                    else:
                        return proc
            except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
                return False

    def start_hashcat(self):
        if self.is_hashcat_running():
            print("Hashcat is already running")
            return
        else:
            print("Hashcat is not running. Starting...")
            command = "hashcat -m 5500 hash_pool.txt -a 3 ?1?1?1?1?1?1?1?1 --increment -1 ?l?d?u"
            self.hashcat_instance = subprocess.Popen(command.split(" "), stdin=subprocess.PIPE)
            print("started, sending status to server")
            self.update_status(new_status="UP")
            print("Start was sucessfull")

    def stop_hashcat(self):
        if self.is_hashcat_running():
            print("Hashcat instance will be stopped")
            self.hashcat_instance.kill()
            #self.hashcat_instance.wait()
            if self.is_hashcat_running():
                print("Still not yet dead.")
            print("Hashcat has been stopped")
            self.update_status(new_status="DOWN")
            return
        else:
            return

    def restart_hashcat(self):
        self.update_status(new_status="RESTART")
        self.stop_hashcat()
        self.start_hashcat()
        print("change status to UP")
        self.update_status(new_status="UP")

    def get_results_from_hashcat(self) -> list:
        api = self.api_url + "/get_hashes_from_queue"
        data = requests.get(api).json()
        hash_list = []
        for result in data:
            hash_list.append(result.get("pk"))
            self.add_new_hash_into_pool(result.get("pk"))
        return hash_list

    def run(self):
        while True:
            try:
                print("\nGetting order.")
                order = self.get_orders()
                print(f"Order is: {order}")
                if order == "start":
                    self.start_hashcat()
                if order == "restart":
                    self.restart_hashcat()
                if order == "stop":
                    self.stop_hashcat()

                print("Getting queue from server")
                self.get_results_from_hashcat()
                print("Checking for new, cracked values")
                self.check_hashcat_results()

                time.sleep(1)
            except Exception as err:
                print(err)
                time.sleep(5)
                continue

    def update_status(self, new_status=None):
        api = self.api_url + "/change_hashcat_status"
        json_request = {"status": new_status}

        print("Update server status with data: {}".format(json_request))
        requests.post(data=json.dumps(json_request), url=api)

    def check_hashcat_results(self):
        command = "hashcat -m 5500 hash_pool.txt -a 3 ?1?1?1?1?1?1?1?1 --increment -1 ?l?d?u --show"
        process = subprocess.Popen(command.split(" "), stdin=subprocess.PIPE,
                                                           stdout=subprocess.PIPE,
                                                           stderr=subprocess.PIPE)

        for result in process.stdout.readlines():
            hash_value = ":".join(result.decode().split(":")[0:-1]).replace("\n", "")
            cracked_value = result.decode().split(":")[-1].replace("\n", "")
            if hash_value not in self.already_sent_results:
                hash_value = hash_value.replace("\n", "")
                requests.post(url=self.api_url + "/add_hash_value", data=json.dumps({"hash": hash_value, "value": cracked_value}))
                requests.post(url=self.api_url + "/edit_hash_status", data=json.dumps({"hash": hash_value, "status": "Cracked"}))
                self.already_sent_results.append(hash_value)


# ToDo: ked sa nastartuje a je stav na servery restart, tak sa zacykly restartovanie
if __name__ == "__main__":
    HashcatWrapper().run()

